import React, {Component} from 'react';
import {TouchableOpacity,
  Text,Button,Actions,
  StyleSheet,
  ScrollView,
  View,Picker
} from 'react-native';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';

export default class ShowDateRelatedData extends Component {
  state: State;
  static navigationOptions = {
       title: 'OBRES EN EXECUCIÓ',
       headerTitleStyle :{textAlign: 'center',alignSelf:'center'},
       headerStyle:{
           backgroundColor:'#9da6a9'
       },
   };
  constructor(props){
  super(props);
  this.state = {
    tableHead: ['Subcontactista'],
    tableData: [
      ['ABC Inc.'],
      ['Test Inc'],
      ['ABC Inc.' ]
    ]
  };
  }

  render() {
const state = this.state;
const { navigation } = this.props;
const day = navigation.getParam('day');
    return (
      <ScrollView style={styles.container}>
      <Text>Edifici Mistral</Text>
      <View style={styles.container_btn}>
      <View style={styles.button}>
        <TouchableOpacity style = {styles.tabButton}  onPress={() => this.props.navigation.navigate('')}>
            <Text style = {styles.tabButtonText}>
               Afegir
            </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.button} >
        <TouchableOpacity style = {styles.tabButton} onPress={() => this.props.navigation.navigate('')} >
              <Text  style = {styles.tabButtonText}>
                Edita
              </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.button} >
        <TouchableOpacity   style = {styles.eliminarButton} onPress={() => this.props.navigation.navigate('')} >
              <Text color="#fff"  style = {styles.eliminarButtonText}>
                Eliminar
              </Text>
        </TouchableOpacity>
      </View>
      </View>
      <View style={styles.container}>
      <Text>Detall del dia {day.dateString}</Text>
        <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
          <Row data={state.tableHead} style={styles.head} textStyle={styles.text}/>
          <TouchableOpacity    >
          <Rows data={state.tableData} onPress={() => this.props.navigation.navigate('addUsersScreen')} textStyle={styles.text}/>
  </TouchableOpacity>
        </Table>
      </View>
      </ScrollView>
    );
  }

}

const styles = StyleSheet.create({
   tabButton: {
      backgroundColor: '#ffca28',
      alignItems: 'stretch',
      height: 50
   },eliminarButton:{
     backgroundColor: '#37474f',
     alignItems: 'stretch',
     height: 50
   },
   eliminarButtonText:{
      color: '#fff',
      fontWeight: '800',
      textAlign: 'center',
      fontSize: 15,
      marginTop: 12
   },
   tabButtonText:{
      color: '#000000',
      fontWeight: '800',
      textAlign: 'center',
      fontSize: 15,
      marginTop: 12
   },
   container_btn: {
     flex: 1,
     flexDirection: 'row',
     justifyContent: 'space-between'
   },button: {
     margin:2,
     marginTop:5,
     backgroundColor: '#ffffff',
     width: '33%',
     height: 42
   },container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: { height: 40, backgroundColor: '#f1f8ff' },
  text: { margin: 6 }
});
