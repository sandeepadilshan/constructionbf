
const images = {
	red: require('./red.png'),
	red_strok: require('./red_stroke.png'),
	green: require('./green.png'),
	green_strok: require('./green_stroke.png'),
  yellow: require('./yellow.png'),
  yellow_strok: require('./yellow_stroke.png'),
  dni: require('./img_dni_new.png'),
};


export default images;
