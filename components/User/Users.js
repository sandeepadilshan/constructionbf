"use strict";

import React, { Component } from 'react';
import {View,ScrollView, Text, TouchableOpacity, TextInput,Button, StyleSheet,Alert,FlatList,Image} from 'react-native';
import Dropdown from 'react-native-modal-select-option';

import { TextButton, RaisedTextButton } from 'react-native-material-buttons';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import Images from "../../img/images.js";



class Users extends Component {

  state: State;
  static navigationOptions = {
       title: 'Treballadors',
       headerTintColor: '#ffffff',
       headerTitleStyle :{textAlign: 'center', alignSelf:'center'},
       headerStyle:{
           backgroundColor:'#37474f',
       },
   };

    constructor(props){
    super(props);
    this.state = {
      data: [
      {name:'Manolo Garcia',dni:'DNI001',company: 'COM1 Inc',auth: '2', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png' ,category:'Oficial',note: '1 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Abel Pasto',dni:'DNI002',company: 'COM2 Inc',auth: '0', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '2 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Juan Morallos',dni:'DNI003',company: 'COM3 Inc', auth: '1', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '3 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Juan Correa',dni:'DNI004',company: 'COM4 Inc',auth: '1', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '4 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Antonio Zapita',dni:'DNI005',company: 'COM5 Inc',auth: '2', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '5 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Juan Correa',dni:'DNI004',company: 'COM4 Inc',auth: '1', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '4 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Antonio Zapita',dni:'DNI005',company: 'COM5 Inc',auth: '1', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '5 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Juan Correa',dni:'DNI004',company: 'COM4 Inc',auth: '1', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '4 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Antonio Zapita',dni:'DNI005',company: 'COM5 Inc',auth: '1', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '5 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Antonio Zapita',dni:'DNI005',company: 'COM5 Inc',auth: '2', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '5 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Juan Correa',dni:'DNI004',company: 'COM4 Inc',auth: '1', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '4 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Antonio Zapita',dni:'DNI005',company: 'COM5 Inc',auth: '1', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '5 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Juan Correa',dni:'DNI004',company: 'COM4 Inc',auth: '1', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '4 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Antonio Zapita',dni:'DNI005',company: 'COM5 Inc',auth: '1', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '5 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Juan Correa',dni:'DNI004',company: 'COM4 Inc',auth: '1', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '4 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Antonio Zapita',dni:'DNI005',company: 'COM5 Inc',auth: '2', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '5 The Modal component is a simple way to present content above an enclosing view'},
        {name:'Manual Rodriguz',dni:'DNI006',company: 'COM6 Inc',auth: '0', dni_front : '../../img/barnasfalt_logo.png',dni_back: '../../img/barnasfalt_logo.png',category:'Oficial',note: '6 The Modal component is a simple way to present content above an enclosing view'},
      ],

    };


  }



  render() {
    const plus_btn = <Text  style = {styles.tabButtonPlus}> + </Text>
    const dni =  <Image style={{width: 30, height: 22 }} source={Images.dni} /> ;
    const red =  <Image style={{width: 15, height: 15, marginTop:5}} source={Images.red} /> ;
    const red_stroke =  <Image style={{width: 15, height: 15, marginTop:5}} source={Images.red_strok} /> ;
    const green =  <Image style={{width: 15, height: 15, marginTop:5}} source={Images.green} /> ;
    const green_stroke =  <Image style={{width: 15, height: 15, marginTop:5}} source={Images.green_strok} /> ;
    const yellow =  <Image style={{width: 15, height: 15, marginTop:5}} source={Images.yellow} /> ;
    const yellow_stroke =  <Image style={{width: 15, height: 15, marginTop:5}} source={Images.yellow_strok} /> ;


    return (

      <ScrollView style={styles.container}>

      <Text style={{flex: 1, padding: 2,marginLeft: 20, height:40,fontWeight: 'bold',
      textAlign:'center', alignSelf:'center',fontSize: 17,marginTop: 10}}> Barnasfalt
      </Text>

      <View style={{ borderBottomColor: 'black',borderBottomWidth: 1, }}/>

      <View style={styles.container_tbl}>

        <FlatList
        data={this.state.data}
        renderItem={({item})=>
        <TouchableOpacity
        onPress={(data) =>  this.openWorkerScreen(item.name,item.dni,item.company,item.dni_front,item.dni_back, item.category,item.note)}>
        <View   style={styles.container_btn}>

        <View style={{flex:0.5 }}  >
         <Text style={{flex: 1, padding: 2,marginLeft: 12,fontWeight: 'bold',fontSize: 17,}}>{item.name}</Text>
        </View>



              <View style={{flex:0.1 }} >
               {item.dni_front && item.dni_back  ? dni : null}
              </View>
              <View style={{flex:0.1 }}  >

              </View>
              <View style={{flex:0.1 }} >
                 {item.auth == '2' ? green : green_stroke}
              </View>
              <View style={{flex:0.1 }} >
                {item.auth == '1' ? yellow : yellow_stroke}
              </View>
              <View style={{flex:0.1 }} >
            {item.auth == '0' ? red : red_stroke}
              </View>
        </View>
        <View style={{ borderBottomColor: 'black',borderBottomWidth: 1, }}/>
</TouchableOpacity>


      }
        keyExtractor={(item, index) => index.toString()}  />
        </View>



        <View style={styles.container_btn}>
              <View style={styles.button}>
              <RaisedTextButton  color="#37474f"
                rippleDuration={600} rippleOpacity={0.54} title='Save'     titleColor='white' />
              </View>
              <View style={styles.button} >
              <RaisedTextButton  color="#37474f"  onPress={() => this.props.navigation.navigate('addUsersScreen')}
                rippleDuration={600} rippleOpacity={0.54} title='Afegir Treballador'     titleColor='white' />
            </View>
        </View>
      </ScrollView>
    );
  }

  openWorkerScreen(name,dni,company,dni_front,dni_back,category,note){
    this.props.navigation.navigate('viewUsersScreen', {name,dni,company,dni_front,dni_back,category,note})
  }

    _onShow(value: boolean): void {
      this.setState({
        isShowingOptions: value,
      });
    }
    _onSelect(item: Object, isShow: boolean): void {
      this.setState({
        isShowingOptions: isShow,
        selectedOption: item,
      });
    }
}
export default Users;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  text: {
    textAlign: 'center',
    borderColor: '#bbb',
    padding: 10,
    backgroundColor: '#eee'
  },
  text_: { margin: 6 },
  row: { flexDirection: 'row', backgroundColor: '#ffffff' },
  btn: { width: 58, height: 18, backgroundColor: '#ffe082',  borderRadius: 2 },
  btnText: { textAlign: 'center', color: '#757575' },
  container_btn: {
    marginTop:5,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  button: {
    margin:2,
    marginTop:5,
    backgroundColor: '#ffffff',
    width: '49%',
    height: 42
  },
  addUserButton:{
    backgroundColor: '#ffca28',
    alignItems: 'stretch',
    height: 45
  },
  // Tab button styles
  tabButton: {
      backgroundColor: '#ffca28',
      alignItems: 'stretch',
      height: 50
   },
   tabButtonText:{
      color: '#000000',
      fontWeight: '800',
      textAlign: 'center',
      fontSize: 15,
      marginTop: 12
   },
   tabButtonTextAddUser:{
      color: '#000',
      fontWeight: '800',
      textAlign: 'center',
      fontSize: 16
   },
   tabButtonPlus:{
      color: '#000',
      fontWeight: '800',
      textAlign: 'center',
      fontSize: 30,
      marginTop: 30
       },
  // table class
container_tbl: { flex: 1,  backgroundColor: '#fff', padding:2 , paddingTop:5},
 head: { height: 40, backgroundColor: '#fffde7' },
 text: { margin: 6 }
})
