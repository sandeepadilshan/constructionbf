import React, {Component} from 'react';
import {TouchableOpacity,Text,Button,Actions,StyleSheet, ScrollView, View,Picker } from 'react-native';

import {Calendar, LocaleConfig} from 'react-native-calendars';
import { TextButton, RaisedTextButton } from 'react-native-material-buttons';
import Dropdown from 'react-native-modal-select-option';
import ModalDropdown from 'react-native-modal-dropdown';

//Calendars localized
LocaleConfig.locales['Ca'] = {
  monthNames: ['Gener','Febrer','Març','Abril','Maig','Juny','Juliol','Agost','Setembre','Octubre','Novembre','Desembre'],
  monthNamesShort: ['Gene.','Febr.','Març','Abril','Mai','Juny','Juli.','Agos.','Sete.','Oct.','Nov.','Des.'],
  dayNames: ['Diumenge','Dilluns','Mimarts','Dimecres','Dijous','Divendres','Dissabte'],
  dayNamesShort: ['Diu.','Dil.','Mim.','Dime.','Dijo.','Diven.','Diss.']
};

LocaleConfig.defaultLocale = 'Ca';


// Set con company name to dropdown
const propsDropdown = {
  defaultValue: {value: 5, label: 'Test F'},
  options: [
    {value: 1, label: 'Test B'},
    {value: 2, label: 'Test C'},
    {value: 3, label: 'Test D'},
    {value: 4, label: 'Test E'},
  ],
  label: 'Seleccionar Obra',
  // animationType: 'animated',
};
type State = {
  selectedOption: Object,
  isShowingOptions: boolean;
};
export default class ShowCalendar extends Component {
  state: State;
  static navigationOptions = {
       title: 'Calendari',
       headerTintColor: '#ffffff',
       headerTitleStyle :{textAlign: 'center',alignSelf:'center'},
       headerStyle:{
           backgroundColor:'#37474f'
       },
   };
  constructor(props){
    super(props);
    this.state = {
      selectedOption: propsDropdown.defaultValue || {value: 0, label: 'Test A'},
            isShowingOptions: false
    };
    this.onDayPress = this.onDayPress.bind(this);
  }

  render(){

    //Showing a dropdown option value
    let {isShowingOptions, selectedOption} = this.state;

    return (
      <ScrollView style={styles.container}>


        <View style={styles.container_btn}>
        <View style={styles.button}>
        <TextButton   color="#c9bc1f" title='Calendari'     titleColor='black' />
        </View>
        <View style={styles.button} >
        <RaisedTextButton  color="#ffca28" onPress={() => this.props.navigation.navigate('usersScreen')}
        rippleDuration={600} rippleOpacity={0.54} title='Treballadors'    titleColor='black' />
        </View>
        </View>


        <View style={{flex: 1, padding: 10, paddingTop: 5}}>
                <Dropdown {...propsDropdown}
                  onSelect={this._onSelect.bind(this)}
                  onShow={this._onShow.bind(this)}
                  isShowingOptions={isShowingOptions}
                  selectedOption={selectedOption}
                />
        </View>

        <Calendar
          style={styles.calendar}
          current={'2018-05-08'}
          minDate={'2018-03-10'}
          maxDate={'2018-08-29'}
          firstDay={1}

          markingType={'custom'}
  markedDates={{
    '2018-05-28': {customStyles: {container: {backgroundColor: '#ffca28',  },
        text: {
          color: 'black',
          fontWeight: 'bold'
        },
      },
    },
    '2018-05-13': {customStyles: {container: {backgroundColor: '#ffca28',  },
        text: {
          color: 'black',
          fontWeight: 'bold'
        },
      },
    },
    '2018-05-29': {customStyles: {container: {backgroundColor: '#ffca28',  },
        text: {
          color: 'black',
          fontWeight: 'bold'
        },
      },
    },}}
          // markedDates={{
          //
          //   '2018-05-23': {selected: true, marked: true,textColor: 'green' },
          //   '2018-05-24': {selected: true, marked: true, dotColor: 'green'},
          //   '2018-05-25': {marked: true, dotColor: 'red'},
          //   '2018-05-26': {marked: true},
          //   '2018-05-27': {disabled: true, activeOpacity: 0}
          // }}


          // disabledByDefault={true}
          hideArrows={false}

          // Handler which gets executed on day press. Default = undefined
          // onDayPress={(day) => {console.log('selected day', day)}}
          onDayPress={(day) =>  this.openDateRelatedScreen(day) }

        />

      </ScrollView>
    );
  }
  openDateRelatedScreen(day){
    this.props.navigation.navigate('ShowDateDataScreen', {day})
    //alert('day: ' + day.dateString)
  }

  _onShow(value: boolean): void {
      this.setState({
        isShowingOptions: value,
      });
    }

    _onSelect(item: Object, isShow: boolean): void {
      this.setState({
        isShowingOptions: isShow,
        selectedOption: item,
      });
    }

  onDayPress(day) {
    this.setState({
      selected: day.dateString
    });
  }

}

const styles = StyleSheet.create({
  calendar: {
    borderTopWidth: 1,
    paddingTop: 5,
    borderBottomWidth: 1,
    borderColor: '#eee',
    height: 350
  },
  text: {
    textAlign: 'center',
    borderColor: '#bbb',
    padding: 10,
    backgroundColor: '#eee'
  },
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  container_btn: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  button: {
    margin:2,
    marginTop:5,
    backgroundColor: '#ffffff',
    width: '49%',
    height: 42
  },
  tabButton: {
      backgroundColor: '#ffca28',

      alignItems: 'stretch',
      height: 50
   },
   tabButtonText:{
      color: '#000000',
      fontWeight: '800',
      textAlign: 'center',
      fontSize: 15,
      marginTop: 12
   }
});
