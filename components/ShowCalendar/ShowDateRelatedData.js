 import {TabNavigator} from "react-navigation";

 import StoppedWorks from "./StoppedWorks.js";
 import PendingWorks from "./PendingWorks.js";
 import ProgressWorks from "./ProgressWorks.js";


 var myTabs = TabNavigator({
    Tab1 : {screen: StoppedWorks},
       Tab2 : {screen: PendingWorks},
          Tab3 : {screen: ProgressWorks}
 },

{
  tabBarPosition : 'bottom',
  animationEnable : true,
  tabBarOptions: {
    activeTintColor: 'red',
  }
}
);

 export default myTabs;
