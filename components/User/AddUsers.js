
import React, { Component } from 'react';
import {View,ScrollView, Text, TouchableOpacity, TextInput,Button, StyleSheet,Image} from 'react-native';
import Dropdown from 'react-native-modal-select-option';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'
import ImagePicker from 'react-native-image-picker';

// Set con company name to dropdown
const propsDropdown = {
  defaultValue: {value: 5, label: 'Test F'},
  options: [
    {value: 1, label: 'Test B'},
    {value: 2, label: 'Test C'},
    {value: 3, label: 'Test D'},
    {value: 4, label: 'Test E'},
  ],
  label: 'Seleccionar Compaña',
  // animationType: 'animated',
};
type State = {
  selectedOption: Object,
  isShowingOptions: boolean;
};


class AddUsers extends Component {

  selectPhotoTappedFront() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource_Front: source
        });
      }
    });
  }

  selectPhotoTappedBack() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({ 
            avatarSource_Back: source
        });
      }
    });
  }

  state = {
      avatarSource_Front: null,
      avatarSource_Back: null,
    dni: '',
    surname: '',
    name: '',
    company: '',
    email: ''
  }
  handleDNI = (text) => {
    this.setState({dni : text})
  }
  handleEmail = (text) => {
     this.setState({ email: text })
  }
  handleCompany = (text) => {
     this.setState({ company: text })
  }
  handleName = (text) => {
     this.setState({ name: text })
  }
  handleSurename = (text) => {
     this.setState({ surname: text })
  }
  saveUsers = (surname, name, dni, email) => {
    if(surname != '' && name != '' && email != '' && dni != ''){
      fetch('http://bmw.qualitapps.com:444/api/services.php?operation=login',{
        method: 'POST',
        header:{
          'Accept': 'application/json',
          'Content-type': 'application/json'
        },
        body:JSON.stringify({
          surname:surname,
          name: name,
          email: email,
          dni: dni
        })
      }).then((response) => response.json()).
      then((responseJSON) =>  {
          alert(responseJSON)
      }).
      catch((error) =>{
        console.error(error);
      });
      alert('Name: ' + name + ' dni : ' + dni + ' surname: ' + surname + ' email: ' + email)
  }
  else{
    alert("Empty Values")
  }
}
  state: State;
  static navigationOptions = {
       title: 'Fitxa de Treballadors',
       headerTitleStyle :{textAlign: 'center',alignSelf:'center'},
       headerStyle:{
           backgroundColor:'white',
       },
   };
   constructor(props) {
     super(props);
     this.state = {
       selectedOption: propsDropdown.defaultValue || {value: 0, label: 'Test A'},
             isShowingOptions: false,
     };

   }
  render() {
    //Showing a dropdown option value
    let {isShowingOptions, selectedOption} = this.state;

    return (
      <ScrollView style={styles.container}>
      <View style={styles.container}>
      <FormLabel style={styles.formLabel}>Cognoms</FormLabel>
      <TextInput style = {styles.input}
         underlineColorAndroid = "transparent"
         placeholderTextColor = "#000000"
         autoCapitalize = "none"
         returnKeyType="go"
         onChangeText = {this.handleSurename}
         ref={(input) => this.passwordInput = input}/>
      <FormLabel style={styles.formLabel}>Noms</FormLabel>
      <TextInput style = {styles.input}
         underlineColorAndroid = "transparent"
         placeholderTextColor = "#000000"
         autoCapitalize = "none"
         returnKeyType="go"
         onChangeText = {this.handleName}
         ref={(input) => this.passwordInput = input}/>
      <FormLabel style={styles.formLabel}>Nombre DNI</FormLabel>
      <TextInput style = {styles.input}
         underlineColorAndroid = "transparent"
         placeholderTextColor = "#000000"
         autoCapitalize = "none"
         returnKeyType="go"
         onChangeText = {this.handleDNI}
         ref={(input) => this.passwordInput = input}/>
      <FormLabel style={styles.formLabel}>Correu electrònic</FormLabel>
      <TextInput style = {styles.input}
         underlineColorAndroid = "transparent"
         placeholderTextColor = "#000000"
         autoCapitalize = "none"
         returnKeyType="go"
         onChangeText = {this.handleEmail}
         ref={(input) => this.passwordInput = input}/>
      <View style={{flex: 1, padding: 2, paddingTop: 5}}>
              <Dropdown {...propsDropdown}
                onSelect={this._onSelect.bind(this)}
                onShow={this._onShow.bind(this)}
                isShowingOptions={isShowingOptions}
                selectedOption={selectedOption}
              />
      </View>
      <View style={styles.container_btn}>
            <View  >
             <Text style={{flex: 1, padding: 2,marginLeft: 12}}>Foto DNI Anvers</Text>
            </View>
             <View style={styles.button_DNI}>
             <TouchableOpacity onPress={this.selectPhotoTappedFront.bind(this)}>
               <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
               { this.state.avatarSource_Front === null ? <Text>Select a Photo</Text> :
                 <Image style={styles.avatar} source={this.state.avatarSource_Front} />
               }
               </View>
             </TouchableOpacity>
             </View>
      </View>
      <View style={styles.container_btn}>
            <View>
            <Text style={{flex: 1, padding: 2,marginLeft: 12}}>Foto DNI Revers</Text>
            </View>
             <View style={styles.button_DNI}>
             <TouchableOpacity onPress={this.selectPhotoTappedBack.bind(this)}>
               <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
               { this.state.avatarSource_Back === null ? <Text>Select a Photo</Text> :
                 <Image style={styles.avatar} source={this.state.avatarSource_Back} />
               }
               </View>
             </TouchableOpacity>
       </View>
      </View>

        <View style={styles.container_btn}>
              <View style={styles.button} >
              <Button color="#37474f"  title={'Cancel'} onPress={() => this.props.navigation.navigate('ShowCalendarScreen')}  />
              </View>
               <View style={styles.button}>
               <Button color="#ffca28"  title={'Guardar'} onPress={() => this.saveUsers(this.state.surname,this.state.name,this.state.dni, this.state.email)}  />
               </View>
        </View>

      </View>

    </ScrollView>
    );
  }
  _onShow(value: boolean): void {
    this.setState({
      isShowingOptions: value,
    });
  }
  _onSelect(item: Object, isShow: boolean): void {
    this.setState({
      isShowingOptions: isShow,
      selectedOption: item,
    });
  }
}
export default AddUsers;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  container_btn: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  button_DNI: {
    margin:2,
    marginTop:5,
    backgroundColor: '#ffffff',
    width: '49%',
    height: 42
  },
  button: {
    margin:2,
    marginTop:5,
    backgroundColor: '#ffffff',
    width: '49%',
    height: 55
  },
  formLabel:{
    color: '#000000',
    fontSize: 30,
    height: 20,
    padding:5,
  },
  input: {
     margin:  2,
     height: 40,
     width: '98%',
     alignItems: 'stretch',
     borderColor: '#4b636e',
     borderWidth: 1
  },

  //Photo avetar
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatar: {
    width: '98%',
    height: 40
  }
})
