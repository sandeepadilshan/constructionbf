//disable the warning masages on bottom of the UI
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

export default class MainService {
  static load(cb){
    setTimeout(cb, 6000);
  }
}
