
import React, { Component } from 'react';
import {View, Text,Button, TouchableOpacity, TextInput, StyleSheet, StatusBar} from 'react-native';
import {  StackNavigator} from 'react-navigation';
import PropTypes from 'prop-types';

class LoginForm extends Component {
  state = {
     email: '',
     password: ''
  }
  handleEmail = (text) => {
     this.setState({ email: text })
  }
  handlePassword = (text) => {
     this.setState({ password: text })
  }
  login_check = () => {

  }
  login = (email, pass) => {
    if(email != '' && pass != ''){ alert('email: ' + email + ' password: ' + pass)
    this.props.navigation.navigate('usersScreen')
   }
  }

   render(){

      return (
         <View style = {styles.container}>
         <StatusBar
          barStyle="light-content"
         />
            <TextInput
               style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Email"
               placeholderTextColor = "#000000"
               autoCapitalize = "none"
               returnKeyType="next"
               onSubmitEditing={() => this.passwordInput.focus()}
               keyBoardType='email-address'
               autoCorrect={false}
               onChangeText = {this.handleEmail}/>

            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Password"
               placeholderTextColor = "#000000"
               autoCapitalize = "none"
               secureTextEntry
               returnKeyType="go"
               onChangeText = {this.handlePassword}
               ref={(input) => this.passwordInput = input}/>

            <TouchableOpacity
               style = {styles.submitButton}
               // onPress = {   () => this.login(this.state.email, this.state.password)
                 onPress = { () =>  navigate('usersScreen')  }>
               <Text style = {styles.submitButtonText}> Login </Text>
             </TouchableOpacity>
         </View>
      );



   }
}

// const AppNavigator = StackNavigator({
//   container
// })

export default LoginForm;


const styles = StyleSheet.create({
   container: {
      paddingTop: 10,
      alignItems: 'stretch',
   },
   input: {
      margin: 15,
      height: 50,
      width:300,
      alignItems: 'stretch',
      borderColor: '#4b636e',
      borderWidth: 1,
      borderRadius: 25,
      paddingLeft: 20
   },
   submitButton: {
      backgroundColor: '#37474f',
      paddingTop: 15,
      alignItems: 'stretch',
      margin: 15,
      height: 50,
      borderRadius: 25
   },
   submitButtonText:{
      color: '#ffffff',
      fontWeight: '800',
      textAlign: 'center',
      fontSize: 15,
   }
})
