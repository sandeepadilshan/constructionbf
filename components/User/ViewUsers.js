
import React, { Component } from 'react';
import {View,ScrollView, Text, TouchableOpacity, TextInput,Button, StyleSheet,Image} from 'react-native';
import Dropdown from 'react-native-modal-select-option';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'
import ImagePicker from 'react-native-image-picker';
import { TextButton, RaisedTextButton } from 'react-native-material-buttons';

import Modal from "react-native-modal";
import Images from "../../img/images.js";

//disable the warning masages on bottom of the UI
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);



class ViewUsers extends Component {

  _renderButton = (text, onPress) => (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.button}>
        <Text>{text}</Text>
      </View>
    </TouchableOpacity>
  );

  _renderModalContent = (note ) => (
    <View style={styles.modalContent}>
      <Text>Hi {note}</Text>
      {this._renderButton("Close", () => this.setState({ visibleModal: null }))}
    </View>
  );

  _renderModalContentShowImg = (image ) => (
    <View style={styles.modalContent}>
    {this._renderButton("Close", () => this.setState({ visibleModal: null }))}
       <Image style={styles.logo} source={require('../../img/barnasfalt_logo.png')}  />
    </View>
  );

  _handleOnScroll = event => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y
    });
  };

  _handleScrollTo = p => {
    if (this.scrollViewRef) {
      this.scrollViewRef.scrollTo(p);
    }
  };

  state: State;
  static navigationOptions = {
       title: 'Fitxa de Treballadors',
       headerTintColor: '#ffffff',
       headerTitleStyle :{ textAlign: 'center',alignSelf:'center'},
       headerStyle:{
           backgroundColor:'#37474f',
       },
   };
   constructor(props) {
     super(props);
     this.state = {
       visibleModal: null
     };
   }
  render() {

    const red =  <Image style={{width: 30, height: 30}} source={Images.red} /> ;
    const red_strok =  <Image style={{width: 30, height: 30}} source={Images.red_strok} /> ;
    const green =  <Image style={{width: 30, height: 30}} source={Images.green} /> ;
    const green_strok =  <Image style={{width: 30, height: 30}} source={Images.green_strok} /> ;

  const state = this.state;
  const { navigation } = this.props;
  const name = navigation.getParam('name');
  const dni  = navigation.getParam('dni');
  const company = navigation.getParam('company');
  const dni_front = navigation.getParam('dni_front');
  const dni_back = navigation.getParam( 'dni_back');
  const category = navigation.getParam( 'category');
  const note = navigation.getParam( 'note');

    //Showing a dropdown option value
    let {isShowingOptions, selectedOption} = this.state;

    return (


      <ScrollView style={styles.container}>

      <View >
      <Modal isVisible={this.state.visibleModal === 1}>
        {this._renderModalContent(note)}
      </Modal>
      </View>

      <View >
      <Modal isVisible={this.state.visibleModal === 2}>
        {this._renderModalContentShowImg(dni_front)}
      </Modal>
      </View>

      <View >
      <Modal isVisible={this.state.visibleModal === 3}>
        {this._renderModalContentShowImg(dni_back)}
      </Modal>
      </View>

      <Text style={{flex: 1, padding: 2,marginLeft: 20, height:40,fontWeight: 'bold',
    textAlign:'center', alignSelf:'center',fontSize: 17,marginTop: 10}}> {name}
    </Text>

        <View style={styles.container}>

         <View style={{ borderBottomColor: 'black',borderBottomWidth: 1, }}/>

         <View style={styles.container_btn}>
               <View style={{flex:0.5 }}  >
               <Text style={{flex: 1, padding: 2,marginLeft: 12}}>Noms</Text>
               </View>
                <View style={{flex:0.5 }}  >
                <Text style={{flex: 1, padding: 2,marginLeft: 12,fontWeight: 'bold'}}> {name}</Text>
                </View>
         </View>



         <View style={{ borderBottomColor: 'black',borderBottomWidth: 1, }}/>

         <View style={styles.container_btn}>
               <View style={{flex:0.5 }}  >
              <Text style={{flex: 1, padding: 2,marginLeft: 12}}>Cognoms</Text>
               </View>
                <View style={{flex:0.5 }}  >
                <Text style={{flex: 1, padding: 2,marginLeft: 12,fontWeight: 'bold'}}> {name}</Text>
                </View>
         </View>

         <View style={{ borderBottomColor: 'black',borderBottomWidth: 1, }}/>

         <View style={styles.container_btn}>
               <View style={{flex:0.5 }} >
              <Text style={{flex: 1, padding: 2,marginLeft: 12}}>Nombre DNI</Text>
               </View>
                <View style={{flex:0.5 }} >
                <Text style={{flex: 1, padding: 2,marginLeft: 12,fontWeight: 'bold'}}> {dni}</Text>
                </View>
         </View>

         <View style={{ borderBottomColor: 'black',borderBottomWidth: 1, }}/>

         <View style={styles.container_btn}>
               <View  style={{flex:0.5 }} >
              <Text style={{flex: 1, padding: 2,marginLeft: 12}}>Correu electrònic</Text>
               </View>
                <View style={{flex:0.5 }}  >
                <Text style={{flex: 1, padding: 2,marginLeft: 12,fontWeight: 'bold'}}> {dni}</Text>
                </View>
         </View>

      <View style={{ borderBottomColor: 'black',borderBottomWidth: 1, }}/>

      <View style={styles.container_veure_btn}>
            <View style={{flex:0.5 }}>
            <Text style={{flex: 1, padding: 2,marginLeft: 12}}>Foto DNI Anvers</Text>
            </View>
            <View style={{flex:0.3 }} >
             <TouchableOpacity style = {styles.veureButton}
             onPress={ ( ) =>   this.setState({ visibleModal: 2 })}
                 // onPress = { () =>  this.props.navigation.navigate('usersScreen')}
               ><Text style = {styles.veureButtonText}> Veure </Text>
             </TouchableOpacity>
            </View>
            <View style={{flex:0.1 }} >
            <Text style={{flex: 1, padding: 2,marginLeft: 12}}>{dni_front ? green : green_strok}</Text>
            </View>
            <View style={{flex:0.1 }} >
            <Text style={{flex: 1, padding: 2,marginLeft: 12,}}> {dni_front ? red_strok  : red} </Text>
            </View>
      </View>

      <View style={{ borderBottomColor: 'black',borderBottomWidth: 1, }}/>

      <View style={styles.container_veure_btn}>
            <View style={{flex:0.5 }}>
            <Text style={{flex: 1, padding: 2,marginLeft: 12}}>Foto DNI Revers</Text>
            </View>
            <View style={{flex:0.3 }} >
             <TouchableOpacity style = {styles.veureButton}
             onPress={ ( ) =>   this.setState({ visibleModal: 2 })}
                 // onPress = { () =>  this.props.navigation.navigate('usersScreen')}
               ><Text style = {styles.veureButtonText}> Veure </Text>
             </TouchableOpacity>
            </View>
            <View style={{flex:0.1 }} >
            <Text style={{flex: 1, padding: 2,marginLeft: 12}}>{dni_back ? green : green_strok}</Text>
            </View>
            <View style={{flex:0.1 }} >
            <Text style={{flex: 1, padding: 2,marginLeft: 12,}}> {dni_back ? red_strok  : red} </Text>
            </View>
      </View>

      <View style={{ borderBottomColor: 'black',borderBottomWidth: 1, }}/>


      <View style={styles.container_btn}>
            <View style={{flex:0.5 }}   >
           <Text style={{flex: 1, padding: 2,marginLeft: 12}}>Categoria</Text>
            </View>
             <View style={{flex:0.5 }}  >
             <Text style={{flex: 1, padding: 2,marginLeft: 12, fontWeight: 'bold'}}> {category}</Text>
             </View>
      </View>

   <View style={{ borderBottomColor: 'black',borderBottomWidth: 1, }}/>

   <View style={styles.container_veure_btn}  >
         <View style={{flex:0.5 }}>
          <Text style={{flex:1, padding: 2,marginLeft: 12}}>Notes</Text>
         </View>
         <View style={{flex:0.3 }} >
          <TouchableOpacity style = {styles.veureButton}
          onPress={ ( ) =>   this.setState({ visibleModal: 1 })}
              // onPress = { () =>  this.setState({ visibleModal: 1 })}
            ><Text style = {styles.veureButtonText}> Veure </Text>
          </TouchableOpacity>
         </View>
         <View style={{flex:0.1 }} >
         <Text style={{flex: 1, padding: 2,marginLeft: 12}}> {note ? green : green_strok} </Text>
         </View>
         <View style={{flex:0.1 }} >
         <Text style={{flex: 1, padding: 2,marginLeft: 12}}> {note ? red_strok  : red} </Text>
         </View>
   </View>

   <View style={{ borderBottomColor: 'black',borderBottomWidth: 1,  }}/>

      </View>

      <View style={styles.container_btn_bottom}>
            <View style={styles.button}>
            <RaisedTextButton  color="#37474f"
              rippleDuration={600} rippleOpacity={0.54} title='Back'     titleColor='#ffffff' />
            </View>
            <View style={styles.button} >
            <RaisedTextButton    color="#37474f"  onPress={() => this.props.navigation.navigate('addUsersScreen')}
              rippleDuration={600} rippleOpacity={0.54} title='Editar Treballador '     titleColor='#ffffff' />
            </View>
      </View>

    </ScrollView>
    );
  }


}
export default ViewUsers;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  textCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  container_btn: {
    marginTop: 5,
    height: 32,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  container_btn_bottom: {
    marginTop: 5,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  container_veure_btn: {
    marginTop: 10,
    height: 35,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  button_DNI: {
    margin:2,
    marginTop:5,
    backgroundColor: '#ffffff',
    width: '49%',
    height: 42
  },
  button: {
    margin:2,
    marginTop:5,
    backgroundColor: '#ffffff',
    width: '49%',
    height: 45
  },
  formLabel:{
    color: '#000000',
    fontSize: 30,
    height: 2,
    marginTop:2
  },
  input: {
     height:40,
     width: '98%',
     alignItems: 'stretch'
  },
  veureButton: {
    borderColor: '#e53935',
     backgroundColor: '#37474f',
     alignItems: 'stretch',
       borderRadius: 30
  },
  veureButtonText:{
     color: '#ffffff',
     fontWeight: '800',
     textAlign: 'center',
     fontSize: 15,
  },

  editorButton: {
    paddingTop: 15,
     backgroundColor: '#ffca28',
     alignItems: 'stretch',
     height: 50,
  },
editorButtonText:{
  color: '#000000',
  fontWeight: '800',
  textAlign: 'center',
  fontSize: 15,
},
  //Photo avetar
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatar: {
    width: '98%',
    height: 40
  },
  modalContent: {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  bottomModal: {
    justifyContent: "flex-end",
    margin: 0
  },
  scrollableModal: {
    height: 300
  },
  scrollableModalContent1: {
    height: 200,
    backgroundColor: "orange",
    alignItems: "center",
    justifyContent: "center"
  },
  scrollableModalContent2: {
    height: 200,
    backgroundColor: "lightgreen",
    alignItems: "center",
    justifyContent: "center"
  }
})
