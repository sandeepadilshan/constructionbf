/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {  Platform,  StyleSheet,  Text,  View,Image} from 'react-native';
import {  StackNavigator} from 'react-navigation';

import Post from './components/Post.js';
import Login from './components/Login/Login.js';
import LoginForm from './components/Login/LoginForm.js';
import ShowCalendar from './components/ShowCalendar/ShowCalendar.js';
import ShowDateData from './components/ShowCalendar/ShowDateRelatedData.js';
import Users from './components/User/Users.js';
import UsersModal from './components/User/UserModal.js';
import ViewUsers from './components/User/ViewUsers.js';
import AddUsers from './components/User/AddUsers.js';
import MainService from './components/Services/MainService.js';
import DNIImagePicker from './components/User/DNIImagePicker.js';


//disable the warning masages on bottom of the UI
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);


export default class App extends React.Component {
state = {
  loaded : false
}
constructor(){
  super();
  MainService.load(v => this.setState({loaded : true}));
}
  render()
  {
    return (
      // <View style={styles.container}>
      //   { this.state.loaded ?   <AppNavigator/> : <Text>Loading...</Text> }
      //  </View>
<AppNavigator/>
    );
  }
}

const AppNavigator = StackNavigator( {
    LoginScreen : { screen : Login,
      navigationOptions: {
    headerStyle: {display:"none"},
    headerLeft: null
  },
},
    LoginFormScreen : { screen : LoginForm},
    ShowCalendarScreen :{screen: ShowCalendar,
      navigationOptions: {
    headerLeft: null
  },},
  usersScreen : { screen : Users,
      navigationOptions: { 
},},
    ShowDateDataScreen :  ShowDateData,
    addUsersScreen : { screen : AddUsers},
    viewUsersScreen : { screen : ViewUsers},
    DNIImagePickerScreen : { screen : DNIImagePicker},
    UsersModalScreen : { screen : UsersModal},
})


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});
