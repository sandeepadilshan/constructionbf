
import React, { Component } from 'react';
import {AppRegistry,  Platform,  StyleSheet,  Text,  View,Image, Button,TouchableOpacity, TextInput} from 'react-native';
import {  StackNavigator} from 'react-navigation';




type Props = {};

export default class Login extends Component<Props> {
  state = {
     email: '',
     password: ''
  }
  handleEmail = (text) => {
     this.setState({ email: text })
  }
  handlePassword = (text) => {
     this.setState({ password: text })
  }
  login = (email, pass) => {
    if(email != '' && pass != ''){
      fetch('http://bmw.qualitapps.com:444/api/services.php?operation=login',{
        method: 'POST',
        header:{
          'Accept': 'application/json',
          'Content-type': 'application/json'
        },
        body:JSON.stringify({
          username: email,
          password: pass
        })
      }).then((response) => response.json).
      then((responseJSON) =>  {
        alert(email+ " Password: "+ pass);
        if(responseJSON == "ok"){
          alert("Successfully Login");
          this.props.navigation.navigate('usersScreen')
        }else {
          alert("Login Failed!")
        }
      }).
      catch((error) =>{
        console.error(error);
      });
      // alert('email: ' + email + ' password: ' + pass)
      // keyboard.dismiss()
    this.props.navigation.navigate('usersScreen')

   }
  }

  render() {
    return (
<View style={styles.container}>

<View style={styles.logoContainer}>
   <Image style={styles.logo} source={require('../../img/barnasfalt_logo.png')}  />
</View>
<Text style={styles.welcome}>
  Benvingut a Barnasfalt
</Text>

<View style={styles.formContainer}>
<TextInput style = {styles.input}
   underlineColorAndroid = "transparent"
   placeholder = "Nom"
   placeholderTextColor = "#000000"
   autoCapitalize = "none"
   returnKeyType="next"
   onSubmitEditing={() => this.passwordInput.focus()}
   keyBoardType='email-address'
   autoCorrect={false}
   onChangeText = {this.handleEmail}/>

<TextInput style = {styles.input}
   underlineColorAndroid = "transparent"
   placeholder = "Contrasenya"
   placeholderTextColor = "#000000"
   autoCapitalize = "none"
   secureTextEntry
   returnKeyType="go"
   onChangeText = {this.handlePassword}
   ref={(input) => this.passwordInput = input}/>

<TouchableOpacity
   style = {styles.submitButton}
   onPress = {   () => this.login(this.state.email, this.state.password)}
     // onPress = { () =>  this.props.navigation.navigate('usersScreen')}
     >
   <Text style = {styles.submitButtonText}> Entrar </Text>
 </TouchableOpacity>

</View>

<Button onPress={() => this.props.navigation.navigate('ShowCalendarScreen')} title="Go"/>
</View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ced7db',
  },
  logoContainer:{
    backgroundColor: '#ced7db',
  },
  formContainer:{
    backgroundColor: '#ced7db',
  },
  welcome: {
    marginTop:20,
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  logo:{width: 150, height: 150,
  },
  input: {
     margin: 15,
     height: 50,
     width:300,
     alignItems: 'stretch',
     borderColor: '#4b636e',
     borderWidth: 1,
     borderRadius: 25,
     paddingLeft: 20
  },
  submitButton: {
     backgroundColor: '#37474f',
     paddingTop: 15,
     alignItems: 'stretch',
     margin: 15,
     height: 50,
     borderRadius: 25
  },
  submitButtonText:{
    color: '#ffffff',
    fontWeight: '800',
    textAlign: 'center',
    fontSize: 15,
  }
});


AppRegistry.registerComponent('Login',()=> Login);
